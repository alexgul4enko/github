import { combineReducers } from 'redux'
import { session as token } from './modules/session'
import { gists } from './modules/gists'
import { navReducer as nav } from './navigator'

const rootReducer = combineReducers({
  nav,
  token,
  gists,
})

export default rootReducer
