import { createStore, applyMiddleware } from 'redux'
import { createEpicMiddleware, combineEpics } from 'redux-observable'
import rootReducer from './root-reducer';
import storageMiddleware from './utils/storage'
import union from 'lodash/union'
import { sessionEpics } from './modules/session'
import { gistsEpics } from './modules/gists'
import { middleware } from './navigator'

import API, { configure as configureApi } from './utils/api'

const epicMiddleware = createEpicMiddleware(
  combineEpics(...union(
    sessionEpics,
    gistsEpics,
  )), {
    dependencies: { API }
  }
)

const createStoreWithMiddleware = applyMiddleware(
  epicMiddleware,
  storageMiddleware,
  middleware,
)(createStore)

export default function configureStore(initialState) {
  const store = createStoreWithMiddleware(rootReducer, initialState)
  configureApi(store)
  return store
}
