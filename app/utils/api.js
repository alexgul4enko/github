import { Observable } from 'rxjs/Rx'
import keys from 'lodash/keys'
import qs from 'qs'
import isEmpty from 'lodash/isEmpty'

export function configure(s) { store = s }

export default function (endpoint) {
  return new API(endpoint)
}

function buildQueryParams(params) {
  if(isEmpty(params)) {
    return ''
  }

  return Object.keys(params).reduce(function(ret, key) {
    let value = params[key]

    if(value !== null && value !== undefined) {
      const query = key + '=' + value
      return ([...ret, query])
    }

    return ret
  }, []).join('&')
}


class API {
  constructor(endpoint) {
    this.endpoint = endpoint
  }

  request(method = 'GET', data, options = {}) {
    const authToken = store.getState().token
    const Authorization = authToken ?  'token '+ authToken : ''
    const query = method == 'GET' && !isEmpty(data) ? ('?' + buildQueryParams(data)) : ''
    let headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      Authorization,
    }
    return fetch(
      `${this.endpoint}${query}`,
        {
          method,
          body: method == 'GET' ? undefined : (JSON.stringify(data)),
          headers,
        }
      )
      .then(response => {
        if (response.status === 401) {
           throw new Error('401')
        }
        if (response.status === 403) {
           throw new Error('403')
        }
        if (response.status === 404) {
          throw new Error('404')
        }
        if (response.status === 500) {
          throw new Error('500')
        }
        // if (response.headers.get('Content-Type') !== 'application/json') {
        //   return ''
        // }
        return response.json()
          .then(function (body) {
            if (response.ok) {
              return body
            }
            // handle errors
            var errors = {}
            keys(body).forEach(key => {
              let eKey = key
              if (key == 'non_field_errors' || key == 'detail' || key == 'errors') {
                eKey = '_error'
              }
              if (Array.isArray(body[key])) {
                errors[eKey] = body[key][0]
              } else {
                errors[eKey] = body[key]
              }
            })

            throw new Error(errors)
          })
      })
  }

  post(data) {
    return this.request('POST', data)
  }
  delete(data) {
    return this.request('DELETE', data)
  }
  patch(data) {
    return this.request('PATCH', data)
  }
  put(data) {
    return this.request('PUT', data)
  }
  get(data) {
    return this.request('GET', data)
  }
}
