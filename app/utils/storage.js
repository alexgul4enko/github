
import _ from 'lodash'
import { AsyncStorage } from 'react-native'

export default function({getState}) {
  return (next) => (action) => {
    const result = next(action)
    AsyncStorage.setItem('state', JSON.stringify(
      _.pick(getState(), [
        'token',
        'gists',
      ])
    ))
    return result
  }
}
