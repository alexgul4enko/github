import C from 'react-native-config'

const isSSL = !!Number(C.USE_SSL)

const apiProtocol = isSSL ? 'https' : 'http'
const wsProtocol = isSSL ? 'wss' : 'ws'

export const API_URL = `${apiProtocol}://${C.API_HOST}/api/v${C.API_VERSION}/`
export const WS_URL = `${wsProtocol}://${C.API_HOST}/chat/`
