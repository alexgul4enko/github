import React, { Component } from 'react'
import { connect } from 'react-redux'
// import { bindActionCreators } from 'redux'
import { StackNavigator, addNavigationHelpers, NavigationActions } from 'react-navigation'
import { createReduxBoundAddListener, createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers'
import { LoginScreen } from './modules/session'
import { GistsView } from './modules/gists'

const appOptions = {
  initialRouteName: 'Home',
  headerMode: 'none',
}

const appRoutes = {
  Home: GistsView,
  Login: LoginScreen,
}

const SessionNavigator = StackNavigator(appRoutes, appOptions);
const initialState = SessionNavigator.router.getStateForAction(SessionNavigator.router.getActionForPathAndParams('Login'))

const navReducer = (state = initialState, action) => {
  const nextState = SessionNavigator.router.getStateForAction(action, state);
  return nextState || state;
}

const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
)
const addListener = createReduxBoundAddListener("root")


class AppWithNavigationState extends Component {

  constructor(props) {
    super(props)
    this.handleUserToken = this.handleUserToken.bind(this)
  }

  handleUserToken(token){
    const reset = NavigationActions.reset({
      index: 0,
      actions:[ NavigationActions.navigate({ routeName: token ? 'Home' : 'Login' })]
    })
    this.props.dispatch(reset)
  }

  componentWillMount() {
    this.handleUserToken(this.props.token)
  }

  componentWillReceiveProps({ token }) {
    if ( token !== this.props.token ) {
      this.handleUserToken(token)
    }
  }

  render() {
    return (
      <SessionNavigator
        navigation={addNavigationHelpers({
          dispatch: this.props.dispatch,
          state: this.props.nav,
          addListener,
        })}
      />
    )
  }
}

export default connect(({ nav, token }) => ({ nav, token }))(AppWithNavigationState)

export { navReducer, middleware }
