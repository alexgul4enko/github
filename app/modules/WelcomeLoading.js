import React from 'react'
import { View, StyleSheet, Image } from 'react-native'
import styles from '../styles'
import Images from '@images/images'

const WelcomeLoading = () => {
  return (
    <View style={style.root}>
      <Image
        source={Images.giphy}
        style={style.cat}
        resizeMode="contain"
      />
    </View>
  )
}
  
const style=StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: styles.DEVICE_WIDTH,
    height: styles.DEVICE_HEIGHT,
  },
  cat: {
    width: styles.DEVICE_WIDTH - styles.FONT_SIZE * 4,
    height: undefined,
    aspectRatio: 800 / 665,
  }
})

export default WelcomeLoading