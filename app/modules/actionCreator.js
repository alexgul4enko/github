import { createAction } from 'redux-actions'

const createAsyncAction = (type) =>
({
  request: createAction(`${type}_REQUEST`),
  success: createAction(`${type}_SUCCESS`),
  failure: createAction(`${type}_FAILURE`),
  update: createAction(`${type}_UPDATE`),
  abort: createAction(`${type}_ABORT`),
  finish: createAction(`${type}_FINISH`),
  toggleLoading: createAction(`${type}_LOADING`),
  add: createAction(`${type}_ADD`),
  loadMore: createAction(`${type}_LOAD_MORE`),
  delete: createAction(`${type}_DELETE`),
  refresh: createAction(`${type}_REFRESH`),
})

export default createAsyncAction