import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity } from  'react-native'
import { CachedImage } from 'react-native-cached-image'
import styles from '../../styles'

export default function GistItem ({ name, owner, description='', onPress, ...rest}) {
  return (
    <TouchableOpacity style={style.repo} onPress={onPress}>
      <CachedImage
        source={{uri: owner.avatar_url}}
        style={style.img}
      />
      <View style={style.content}>
        <Text
          style={style.title}
          numberOfLines={1}
          ellipsizeMode="tail"
        >
          {name}
        </Text>
        {description && (<Text style={style.desctiption}>{description.slice(1,30)}{ description.length > 30 ? '...' : ''}</Text>)}
      </View>
    </TouchableOpacity>
  )
}

const style = StyleSheet.create({
  repo: {
    flexDirection: 'row',
    marginVertical: styles.FONT_SIZE / 2,
    height: styles.FONT_SIZE * 4
  },
  img: {
    width: styles.FONT_SIZE * 3,
    height: styles.FONT_SIZE * 3,
    borderRadius: 4,
  },
  content: {
    flex: 1,
    paddingLeft: styles.FONT_SIZE,
  },
  title: {
    fontSize: styles.FONT_SIZE * 1.2,
    color: 'blue',
  },
  desctiption: {
    fontSize: styles.FONT_SIZE,
    paddingVertical: styles.FONT_SIZE/2,
    color:  styles.COLOR_FONT,
  }
})
