import React, { Component } from 'react'
import { connect } from 'react-redux'
import { gistActions } from './gists'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Image, FlatList, NetInfo, Modal, WebView } from 'react-native'
import Images from '@images/images'
import GistItem from './GistItem'
import styles from '../../styles'
import isEmpty from 'lodash/isEmpty'

class GistsView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      orderBy: undefined,
      isConnected: true,
      url: ''
    }

    this.loadMore = this.loadMore.bind(this)
    this._renderItem = this._renderItem.bind(this)
  }

  componentWillMount() {
    NetInfo.isConnected.fetch().then(isConnected => {
      this.setState({ isConnected })
    })
  }

  fetchResults() {
    const { search, orderBy } = this.state
    if(!search) return this.props.abort()
    const sort = orderBy ? {sort: orderBy, order: 'asc'} : {}
    this.props.request({
      q: `${search}+in:name`,
      ...sort
    })
  }

  loadMore({distanceFromEnd}) {
    this.props.loadMore()
  }

  search(key, value) {
    this.setState({ [key]: value }, this.fetchResults)
  }

  orderBy(key) {
    const { orderBy } = this.state
    this.setState({ orderBy: orderBy===key ? undefined : key }, this.fetchResults)
  }

  _keyExtractor(item) {
    return item.id + ''
  }

  _renderItem({item}) {
    return (<GistItem {...item} onPress={this.toggleModal.bind(this,item.html_url)}/>)
  }

  _getItemLayout(data, index) {
    return {
      length: styles.FONT_SIZE * 4,
      offset: styles.FONT_SIZE * 4 * index,
      index
    }
  }

  toggleModal(url) {
    this.setState({ url })
  }

  render() {
    const { search, orderBy, isConnected, url } = this.state
    const { data, errors } = this.props.gists
    return (
      <View style={style.root}>
        <View style={style.header}>
          <View style={style.search}>
            <Text style={style.text}>Search by name</Text>
            <TextInput
              style={style.input}
              value={search}
              onChangeText={this.search.bind(this, 'search')}
              placeholder="Search"
              placeholderTextColor="#ccc"
              underlineColorAndroid="transparent"
            />
          </View>
          <View style={style.order}>
            <Text style={style.text}>Order by</Text>
            <View style={style.row}>
              <TouchableOpacity style={style.btn} onPress={this.orderBy.bind(this, 'stars')}>
                <Image
                  source={Images.star}
                  style={[style.img, orderBy !== 'stars' ? style.disabled : null  ]}
                  resizeMode="center"
                />
              </TouchableOpacity>
              <TouchableOpacity style={style.btn} onPress={this.orderBy.bind(this,'forks')}>
                <Image
                  source={Images.fork}
                  style={[style.img, orderBy !== 'forks'? style.disabled : null  ]}
                  resizeMode="center"
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={style.listWrapper}>
          <FlatList
            style={style.list}
            data={data.items}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            ItemSeparatorComponent={()=>(<View style={style.divider}/>)}
            ListEmptyComponent={(<Text style={style.message}>{
              search ? 'no data' : isConnected ? 'type to search' : 'offline'
            }</Text>)}
            onEndReached={this.loadMore}
            onEndReachedThreshold={0.3}
            getItemLayout={this._getItemLayout}
          />
        </View>
        <Modal
          visible={!!url}
          animationType='fade'
          onRequestClose={this.toggleModal.bind(this,'')}
          transparent
        >
          <View style={style.modalContent}>
            <View style={style.headerModal}>
              <TouchableOpacity style={style.closebtn} onPress={this.toggleModal.bind(this,'')}>
                <Text style={style.close}>X</Text>
              </TouchableOpacity>
            </View>
            <WebView
              source={{uri: url}}
              style={style.webview}
            />
          </View>
        </Modal>
      </View>
    )
  }
}

const style = StyleSheet.create({
  root: {
    width: styles.DEVICE_WIDTH,
    height: styles.DEVICE_HEIGHT,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column',
  },
  modalContent: {
    width: styles.DEVICE_WIDTH,
    height: styles.DEVICE_HEIGHT,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    backgroundColor: 'rgba(51,51,51,0.7)',
  },
  header: {
    alignSelf: 'stretch',
    height: styles.HEADER_HEIGHT,
    backgroundColor: styles.COLOR_PRIMARY,
    flexDirection: 'row',
  },
  headerModal: {
    alignSelf: 'stretch',
    height: styles.HEADER_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  search: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'stretch',
  },
  input: {
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: '#FFFFFF',
    margin: styles.FONT_SIZE / 2,
    borderRadius: 4,
    paddingLeft: styles.FONT_SIZE / 2,
    paddingVertical: 0,
  },
  order: {
    flexDirection: 'column',
    alignSelf: 'stretch',
    flex: 0.3,
    paddingHorizontal: styles.FONT_SIZE / 2
  },
  disabled: {
    tintColor: '#ccc',
  },
  row: {
    flexDirection: 'column',
    alignSelf: 'stretch',
    flexDirection: 'row',
    flexWrap: 'nowrap',
    paddingVertical: styles.FONT_SIZE / 2
  },
  text: {
    fontSize: styles.FONT_SIZE_SMALL,
    color: '#FFFFFF',
    padding: styles.FONT_SIZE / 2,
  },
  btn: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: styles.FONT_SIZE / 2,
  },
  img: {
    width: styles.FONT_SIZE,
    height: styles.FONT_SIZE
  },
  listWrapper: {
    flex: 1,
    alignSelf: 'stretch',
    padding: styles.FONT_SIZE,
    justifyContent: 'center',
  },
  message: {
    fontSize: styles.FONT_SIZE,
    color: styles.COLOR_PRIMARY,
    textAlign: 'center',
  },
  list: {
    flex: 1,
    alignSelf: 'stretch',
  },
  divider: {
    alignSelf: 'stretch',
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: styles.COLOR_PRIMARY,
  },
  webview: {
    flex: 1,
    alignSelf: 'stretch',
    marginBottom: styles.HEADER_HEIGHT * 0.8,
    marginHorizontal: styles.FONT_SIZE * 1.5,
  },
  close: {
    fontSize: styles.FONT_SIZE * 1.5,
    paddingHorizontal: styles.FONT_SIZE * 1.5,
    color: '#FFFFFF'
  },
  closebtn: {
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  }
})

export default connect(({ gists })=>({ gists }), gistActions)(GistsView)
