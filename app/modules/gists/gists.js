import { handleActions } from 'redux-actions'
import { combineReducers } from 'redux'
import { fromPromise } from 'rxjs/observable/fromPromise'
import { of } from 'rxjs/observable/of'
import createAsyncAction from '../actionCreator'
import get from 'lodash/get'


const gistActions = createAsyncAction('GISTS')

const defailState = {
  items: [],
  total_count: 0,
}

const data = handleActions({
  [gistActions.success().type]: (state, {payload}) => payload,
  [gistActions.add().type]: (state, {payload}) => ({
    ...state,
    ...payload,
    items: [...state.items, ...payload.items]
  }),
  [gistActions.abort().type]:(state)=> defailState,
}, defailState)

const error = handleActions({
  [gistActions.failure().type]: (state, {payload}) => payload,
}, '')

const getEpic = (action$, store, { API }) => {
  return action$
    .ofType(gistActions.request().type)
    .switchMap(function({ payload }) {
      const query = {...payload, per_page: 15, page: 1 } 
      return fromPromise(API('https://api.github.com/search/repositories').get(query))
        .debounceTime(250)
        .takeUntil(action$.ofType(gistActions.abort().type))
        .switchMap(({ items, total_count }) => {
          return of(gistActions.success({ items, total_count, query }))
        })
        .catch(err => ([]))
    })
}

const loadMoreEpic = (action$, store, { API }) => {
  return action$
    .ofType(gistActions.loadMore().type)
    .switchMap(function() {
      let { query, total_count } = store.getState().gists.data
      if(!query || total_count <= get(query, 'page', 0) * 15) return []
      query.page = query.page + 1
      return fromPromise(API('https://api.github.com/search/repositories').get(query))
        .switchMap(({ items, total_count }) => {
          return of(gistActions.add({ items, query }))
        })
        .catch(err => ([]))//to do edd errors
    })
}

const gists = combineReducers({
  data,
  error,
})

const gistsEpics = [ getEpic, loadMoreEpic ]
export { gistsEpics, gists, gistActions }
