import { handleActions } from 'redux-actions'
import { fromPromise } from 'rxjs/observable/fromPromise'
import { of } from 'rxjs/observable/of'
import createAsyncAction from '../actionCreator'
import { Linking } from 'react-native'


const authActions = createAsyncAction('SESSION_TOKEN')
const client_id = '062fe448d781d714a71f'
const client_secret = '54dc062c7cd9fb34dfdecb46341e385d8b5e634b'

function parseUrl(str) {
  return str
    .split('&')
    .reduce(function(params, param) {
      const paramSplit = param.split('=').map(function (chunk) {
        return decodeURIComponent(chunk.replace('+', '%20'))
      });
      const name = paramSplit[0]
      let value = paramSplit[1]
      return ({...params, [name] : value })
    }, {})
}

function Oauth() {
  return new Promise((resolve, reject)=> {
    function handleUrl (event) {
      Linking.removeEventListener('url', handleUrl)
      resolve(parseUrl(event.url.split('?').pop()))
    }
    Linking.addEventListener('url',handleUrl)
    Linking.openURL([
      'https://github.com/login/oauth/authorize?response_type=token',
      `&client_id=${client_id}`,
      '&scope=gist',
      `&redirect_uri=${encodeURIComponent('github-gists://')}`
      ].join(''))
  })
}


const session = handleActions({
  [authActions.success().type]: (state, {payload}) => payload,
  [authActions.abort().type]:(state)=>'',
}, '')


const signInEpic = (action$, store, { API }) => {
  return action$
    .ofType(authActions.request().type)
    .switchMap(function(action) {
      return fromPromise(Oauth()
        .then(oauth =>{
          return API('https://github.com/login/oauth/access_token').post({...oauth, client_id, client_secret, accept:'json' })
        }))
        .switchMap(({access_token}) => {
          return of(authActions.success(access_token))
        })
        .catch(err => ([]))//to do edd errors
    })
}

const sessionEpics = [ signInEpic ]
export { sessionEpics, session, authActions }
