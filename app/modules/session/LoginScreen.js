import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native'
import styles from '../../styles'
import Images from '@images/images'
import { authActions } from './session'

export class LoginScreen extends Component {
  render() {
    return (
      <View style={style.root}>
        <Image
          source={Images.cat}
          style={style.cat}
          resizeMode="contain"
        />
        <TouchableOpacity style={style.btn} onPress={this.props.request}>
          <Image
            source={Images.github}
            style={style.github}
            resizeMode="center"
          />
          <Text style={style.text}>Sign in with GitHub</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const style = StyleSheet.create({
  root: {
    width: styles.DEVICE_WIDTH,
    height: styles.DEVICE_HEIGHT,
    backgroundColor: '#FFFFFF',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingHorizontal: styles.FONT_SIZE * 2,
  },
  cat: {
    width: undefined,
    height: undefined,
    alignSelf: 'stretch',
    aspectRatio: 896 / 896,
  },
  btn: {
    height: styles.FONT_SIZE * 3,
    borderRadius: styles.FONT_SIZE * 1.5,
    backgroundColor: styles.COLOR_PRIMARY,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: styles.FONT_SIZE * 2,
    alignSelf: 'stretch'
  },
  text: {
    color: '#FFFFFF',
    fontSize: styles.FONT_SIZE,
    paddingLeft: styles.FONT_SIZE,
  },
  github: {
    height: styles.FONT_SIZE * 1.5,
    width: styles.FONT_SIZE * 1.5,
  }
})

export default connect(()=>({}), authActions )(LoginScreen)
