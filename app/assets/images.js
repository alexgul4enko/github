export default {
  cat: require('./img/cat.jpeg'),
  octocat: require('./img/Octocat.png'),
  giphy: require('./img/giphy.gif'),
  github: require('./img/github.png'),
  star: require('./img/star.png'),
  fork: require('./img/fork.png'),
}
