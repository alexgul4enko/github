import React, { Component } from 'react'
import WelcomeLoading from './modules/WelcomeLoading'
import { Provider } from 'react-redux'
import configureStore from './store'
import { AsyncStorage, SafeAreaView } from 'react-native'
import App from './navigator'


export default class StoryToldApp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      store: null
    }
    AsyncStorage.getItem('state')
      .then(data=> {
        this.setState({
          store: configureStore( data ? JSON.parse(data) : undefined )
        })
      })
  }
  render() {
    if(!this.state.store) return <WelcomeLoading/>
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
        <Provider store={this.state.store}>
          <App />
        </Provider>
      </SafeAreaView>
    )
  }
}
